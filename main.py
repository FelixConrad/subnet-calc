import math


def subnetCalc():
    print("Netzadresse in 000.000.000.000 Format:")
    basis_netz_string = input()
    basis_netz = []
    for octet in basis_netz_string.split("."):
        basis_netz.append(int(octet))
    print("Anzahl gewünschter Subnetze:")
    anz_subnetz = int(input())
    log_anz_subnetz = math.log2(anz_subnetz)
    log_anz_subnetz = int(log_anz_subnetz) +1  if log_anz_subnetz % 1 != 0 else int(log_anz_subnetz)
    host_anz_subnetz = int(math.pow(2,8-log_anz_subnetz))
    print(f'Anzahl der möglichen Hosts pro Netz: {host_anz_subnetz-2}')
    print(f'Subnet Mask: 255.255.255.{256-host_anz_subnetz}')
    for i in range(0,anz_subnetz):
        print(f'{i+1}.Netz: {basis_netz[0]}.{basis_netz[1]}.{basis_netz[2]}.{host_anz_subnetz*i}')



if __name__ == '__main__':
    wiederhole = True
    while wiederhole:
        subnetCalc()
        print("Erneut berechnen? (ja/nein)")
        result = input()
        if result.lower() == "n" or result.lower() == "nein":
            wiederhole = False